﻿// Arthur Martins Thomé
// 29 APR 2019

#region Statements

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#endregion

namespace Alura.Loja.Testes.ConsoleApp
{
    public class Promocao
    {
        #region Properties

        public int Id { get; set; }
        public string Descricao { get; internal set; }
        public DateTime DataInicial { get; internal set; }
        public DateTime DataFinal { get; internal set; }
        public IList<PromocaoProduto> Produto { get; internal set; }

        #endregion

        #region Constructor

        public Promocao ( )
        {
            Produto = new List<PromocaoProduto> ( );
        }

        #endregion

        public void IncluirProduto ( Produto _produto )
        {
            Produto.Add ( new PromocaoProduto ( ) { Produto = _produto } );
        }
        }
}
