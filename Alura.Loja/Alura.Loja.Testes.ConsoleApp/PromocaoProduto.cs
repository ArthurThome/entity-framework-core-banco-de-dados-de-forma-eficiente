﻿// Arthur Martins Thomé
// 29 APR 2019


namespace Alura.Loja.Testes.ConsoleApp
{
    public class PromocaoProduto
    {
        #region Properties

        public int ProdutoId { get; set; }
        public int PromocaoId { get; set; }
        public Produto Produto { get; set; }
        public Promocao Promocao { get; set; }

        #endregion
    }
}
