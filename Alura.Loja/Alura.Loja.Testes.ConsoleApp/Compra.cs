﻿// Arthur Martins Thomé
//29 APR 2019

using System.ComponentModel.DataAnnotations;

namespace Alura.Loja.Testes.ConsoleApp
{
    public class Compra
    {
        #region Properties

        public int Id { get; internal set; }
        public int ProdutoId { get; internal set; }
        public Produto Produto { get; internal set; }
        public int Quantidade { get; internal set; }
        public double Preco { get; internal set; }

        #endregion

        #region Constructor

        public Compra()
        {
        }

        #endregion

        public override string ToString ( )
        {
            return $"Comrpa de { Quantidade } do produto { Produto.Nome }";
        }
    }
}