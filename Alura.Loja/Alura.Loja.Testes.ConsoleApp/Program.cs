﻿// Arthur Martins Thomé
// 29 APR 2019

#region Statements

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#endregion

namespace Alura.Loja.Testes.ConsoleApp
{
    class Program
    {

        #region Fields

        #endregion

        #region Main

        static void Main ( string [ ] args )
        {
            RelacionamentoUmParaMuitos ( );
        }

        #endregion

        #region Primeira Aula

        private static void PrimeiraAula ( )
        {
            using ( var context = new LojaContext ( ) )
            {
                Loggar ( context );

                var produtos = context.Produtos.ToList ( );

                ExibeEntries ( context.ChangeTracker.Entries ( ) );

                //var p1 = produtos.First();
                //p1.Nome = "Livro do Harry Potter";

                var novoProduto = new Produto ( )
                {
                    Nome = "Bolacha",
                    Categoria = "Comida",
                    PrecoUnitario = 1.50
                };

                context.Produtos.Add ( novoProduto );

                context.Produtos.Remove ( novoProduto );

                ExibeEntries ( context.ChangeTracker.Entries ( ) );

                context.SaveChanges();

                ExibeEntries(context.ChangeTracker.Entries());
            }
        }

        #endregion

        #region Muitos para Muitos

        private static void MuitosParaMuitos ( )
        {
            var p1 = new Produto() { Nome = "Suco de Laranja", Categoria = "Bebidas", PrecoUnitario = 8.79, Unidade = "Litros" };
            var p2 = new Produto() { Nome = "Café", Categoria = "Bebidas", PrecoUnitario = 12.45, Unidade = "Gramas" };
            var p3 = new Produto() { Nome = "Macarrão", Categoria = "Alimentos", PrecoUnitario = 4.23, Unidade = "Gramas" };

            var promocaoPascoa = new Promocao();
            promocaoPascoa.Descricao = "Páscoa Feliz";
            promocaoPascoa.DataInicial = DateTime.Now;
            promocaoPascoa.DataFinal = DateTime.Now.AddMonths(3);
            promocaoPascoa.IncluirProduto(p1);
            promocaoPascoa.IncluirProduto(p2);
            promocaoPascoa.IncluirProduto(p3);


            using ( var context = new LojaContext ( ) )
            {
                //loggar sql no console
                Loggar ( context );
                
                
                //context.Promocoes.Add ( promocaoPascoa );
                //excluir promocao
                var promocao = context.Promocoes.Find ( 2 );
                context.Promocoes.Remove ( promocao ); 

                ExibeEntries ( context.ChangeTracker.Entries ( ) );

                context.SaveChanges ( );
            }
        }

        #endregion

        #region Um para Muitos

        private static void UmParaMuitos ( )
        {
            var paoFrances = new Produto ( );
            paoFrances.Nome = "Pão Frances";
            paoFrances.PrecoUnitario = 0.40;
            paoFrances.Unidade = "Unidade";
            paoFrances.Categoria = "Padaria";

            var compra = new Compra ( );
            compra.Quantidade = 6;
            compra.Produto = paoFrances;
            compra.Preco = paoFrances.PrecoUnitario + compra.Quantidade;

            using ( var context = new LojaContext ( ) )
            {
                //loggar sql no console
                Loggar ( context );

                context.Compras.Add ( compra );

                ExibeEntries ( context.ChangeTracker.Entries ( ) );

                context.SaveChanges ( );
            }
        }

        #endregion

        #region UmParaUm

        private static void UmParaUm ( )
        {
            var fulano = new Cliente ( );
            fulano.Nome = "Fulano de tal";
            fulano.EnderecoEntrega = new Endereco ( )
            {
                Numero = 12,
                Bairro = "Rua dos inválidos",
                Cidade = "Sobrado",
                Logradouro = "Centro",
                Complemento = "Cidade"
            };

            using ( var context = new LojaContext ( ) )
            {
                Loggar ( context );

                context.Clientes.Add ( fulano );
                context.SaveChanges ( );
            }
        }

        #endregion

        #region Recuperando Objetos Relacionados M para M

        private static void RecuperandoMParaM ( )
        {
            //using ( var context = new LojaContext ( ) )
            //{
            //    Loggar ( context );

            //    var promocao = new Promocao ( );
            //    promocao.Descricao = "Queima Total janeiro 2019";
            //    promocao.DataInicial = new DateTime ( 2019, 1, 1 );
            //    promocao.DataFinal = new DateTime ( 2019, 1, 31 );

            //    var produtos = context.Produtos.Where ( p => p.Categoria == "Bebidas" ).ToList ( ) ;

            //    foreach ( var item in produtos )
            //    {
            //        promocao.IncluirProduto ( item );
            //    }

            //    context.Promocoes.Add ( promocao );

            //    ExibeEntries ( context.ChangeTracker.Entries ( ) );

            //    context.SaveChanges ( );
            //}

            using ( var context2 = new LojaContext ( ) )
            {
                Loggar ( context2 );

                //fazer um select join, usa esse metodo include
                //descer mais niveis no relacionamento, usa o thenInclude
                var promocao = context2.Promocoes.Include ( p => p.Produto ).ThenInclude ( pp => pp.Produto ).FirstOrDefault ( );
                Console.WriteLine ( "mostrando produtos da promocao..." );

                foreach ( var item in promocao.Produto )
                    Console.WriteLine ( item.Produto );
            }
        }

        #endregion

        #region Recuperando Objetos Relacionados Um para UM

        private static void RecuperandoUmParaUm ( )
        {
            using ( var context = new LojaContext ( ) )
            {
                Loggar ( context );

                var cliente = context.Clientes.Include ( c => c.EnderecoEntrega ).FirstOrDefault ( );
                Console.WriteLine ( $"Endereco de Entrega: { cliente.EnderecoEntrega.Logradouro }" );
            }
        }

        #endregion

        #region Recuperando Objetos Relacionados Um para Muitos

        private static void RelacionamentoUmParaMuitos ( )
        {
            using ( var context = new LojaContext ( ) )
            {
                Loggar ( context );

                var produto = context.Produtos.Include ( p => p.Compras ).Where ( p => p.Id == 8 ).FirstOrDefault ( );

                context.Entry ( produto ).Collection ( p => p.Compras ).Query ( ).Where ( c => c.Preco > 10 ).Load ( );

                Console.WriteLine ( $"Mostrando as compras do produto { produto.Nome }" );
                foreach ( var item in produto.Compras )
                    Console.WriteLine ( item );

            }
        }

        #endregion

        private static void Loggar ( LojaContext _context )
        {
            var serviceProvider = _context.GetInfrastructure<IServiceProvider> ( );
            var loggerFactory = serviceProvider.GetService<ILoggerFactory> ( );
            loggerFactory.AddProvider ( SqlLoggerProvider.Create ( ) );
        }

        private static void ExibeEntries ( IEnumerable<EntityEntry> entries )
        {
            Console.WriteLine ( "==============================" );
            foreach ( var p in entries )
            {
                Console.WriteLine ( p.Entity.ToString ( ) + " - " + p.State );
            }
        }
    }
}
