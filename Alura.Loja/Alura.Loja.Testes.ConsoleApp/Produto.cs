﻿// Arthur Martins Thomé
// 29 APR 2019

using System.Collections.Generic;

namespace Alura.Loja.Testes.ConsoleApp
{
    public class Produto
    {
        #region Properties

        public int Id { get; internal set; }
        public string Nome { get; internal set; }
        public string Categoria { get; internal set; }
        public double PrecoUnitario { get; internal set; }
        public string Unidade { get; internal set; }
        public IList<PromocaoProduto> Promocoes { get; set; }
        public IList<Compra> Compras { get; set; }

        #endregion

        #region override

        public override string ToString ( )
        {
            return $"Produto: {Id}, {Nome}, {Categoria}, {PrecoUnitario}";
        }

        #endregion
    }
}