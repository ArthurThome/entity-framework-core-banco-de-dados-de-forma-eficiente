﻿// Arthur Martins Thomé
// 30 APR 2019

#region Statementes

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


#endregion

namespace Alura.Loja.Testes.ConsoleApp
{
    public class Cliente
    {
        #region Properties

        public int Id { get; set; }
        public string Nome { get; internal set; }
        public Endereco EnderecoEntrega { get; set; }

        #endregion

        #region Constructor

        #endregion
    }
}
