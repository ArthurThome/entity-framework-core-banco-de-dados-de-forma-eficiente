﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alura.Loja.Testes.ConsoleApp
{
    class ProdutoDAOEntity : IProdutoDAO, IDisposable
    {
        #region Fields

        private LojaContext context;

        #endregion

        #region Constructor

        public ProdutoDAOEntity ( )
        {
            this.context = new LojaContext ( );
        }

        #endregion

        #region CRUD Methods

        public void Adicionar ( Produto p )
        {
            try
            {
                context.Produtos.Add ( p );
                context.SaveChanges ( );
            }
            catch
            {
                throw new NotImplementedException ( );
            }
        }

        public void Atualizar ( Produto p )
        {
            try
            {
                context.Produtos.Update ( p );
                context.SaveChanges ( );
            }
            catch
            {
                throw new NotImplementedException ( );
            }
        }

        public void Dispose ( )
        {
            try
            {
                context.Dispose ( );
            }
            catch
            {
                throw new NotImplementedException ( );
            }
        }

        public IList<Produto> Produtos ( )
        {
            try
            {
                return context.Produtos.ToList ( );
            }
            catch
            {
                throw new NotImplementedException ( );
            }
        }

        public void Remover ( Produto p )
        {
            try
            {
                context.Produtos.Remove ( p );
                context.SaveChanges ( );
            }
            catch
            {
                throw new NotImplementedException ( );
            }
        }

        #endregion
    }
}
