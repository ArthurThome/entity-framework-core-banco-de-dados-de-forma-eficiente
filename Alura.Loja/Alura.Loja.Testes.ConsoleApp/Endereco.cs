﻿// Arthur Martins Thomé
// 30 APR 2019

#region Statementes

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


#endregion

namespace Alura.Loja.Testes.ConsoleApp
{
    public class Endereco
    {
        #region Properties

        public int Numero { get; internal set; }
        public string Logradouro { get; internal set; }
        public string Complemento { get; internal set; }
        public string Bairro { get; internal set; }
        public string Cidade { get; internal set; }
        public Cliente Cliente { get; set; }

        #endregion

        #region Constructor

        //public Endereco ( int _numero, string _logradouro, string _complemento, string _bairro, string _cidade )
        //{
        //    Numero = _numero;
        //    Bairro = _bairro;
        //    Cidade = _cidade;
        //    Logradouro = _logradouro;
        //    Complemento = _complemento;
        //}

        #endregion
    }
}
