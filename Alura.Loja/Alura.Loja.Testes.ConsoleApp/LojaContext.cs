﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Alura.Loja.Testes.ConsoleApp
{
    public class LojaContext : DbContext
    {
        #region Properties

        public DbSet<Compra> Compras { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Promocao> Promocoes { get; set; }
        //public DbSet<Endereco> Enderecos { get; set; }

        #region Methods 

        //executado no evento de criacao do modelo
        protected override void OnModelCreating ( ModelBuilder modelBuilder )
        {
            //para fazer chave primaria composta
            modelBuilder.Entity<PromocaoProduto> ( ). HasKey ( pp => new { pp.PromocaoId, pp.ProdutoId } );

            //nome da tabela
            modelBuilder.Entity<Endereco> ( ).ToTable ( "Enderecos" );
            
            //shadow property
            modelBuilder.Entity<Endereco> ( ).Property<int> ( "ClienteId" );

            //montar chave primaria
            modelBuilder.Entity<Endereco> ( ).HasKey ( "ClienteId" );
        }

        #endregion

        #endregion

        protected override void OnConfiguring ( DbContextOptionsBuilder optionsBuilder )
        {
            optionsBuilder.UseSqlServer ( "Server=(localdb)\\mssqllocaldb;Database=LojaDB;Trusted_Connection=true;" );
        }
    }
}